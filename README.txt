/* $Id$ */

Module that integrates the services of Aviary (http://www.aviary.com) into a Drupal installation.

Installation
------------
1. Copy aviary.module to your module directory and then enable it on the admin modules page.
2. Go to http://www.aviary.com/html5builder and get a aviary API Key
3. Enter the Key on the settings page admin/settings/aviary
4. Enter a path for temporary images
5. Select on which content types the module should look for images
6. Grant permission for users

User permissions
----------------
There are three levels of permissions:
1. Administer aviary: Change settings of the aviary module
2. Edit all images: Edit all images on a site
3. Edit own images: Only edit images on nodes where the user has the edit permission

Note: To grant a user access to edit all images you also have to give him the "access own images" permission!

Author
------
Jit Ray Chowdhury
jit.ray.c@gmail.com

