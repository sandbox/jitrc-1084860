// $Id$

if (Drupal.jsEnabled) {
 var _featherLoaded = false; 
 var _featherEditorLoaded = false; 
 var image_stat="O";
 var imagecache=false;
 	   Feather_APIKey =  '62a1cc7e2d43094b8caf60127cc3374e';//default
	   Feather_OpenType = 'lightbox'; //default
      Feather_Theme = 'bluesky'; 
      Feather_EditOptions = 'all'; 
      Feather_CropSizes = '320x240,640x480,800x600,1280x1024'; 
      Feather_Stickers = []; 

	  
	  Feather_OnSave = function(id,new_url) { 
	  _featherEditorLoaded=false;
	  var hostname = window.location.hostname;
	  hostname = hostname.replace("www.","").toLowerCase();

      old_url=$('#'+id).attr("src");
	  var height= $('#'+id).height();
	  var width= $('#'+id).width();
	 // alert("old "+height+" by "+width);
	 var external=false;
	  if(old_url.indexOf("http://")!=-1 && old_url.indexOf(hostname)==-1){external=true;}//alert("external");
	  
	  old_url=old_url.replace("http://"+location.host+"/", "");
      loading_url=Drupal.settings.aviary_image_editor_loading_url;
		$('#'+id).removeAttr("width"); 
		$('#'+id).removeAttr("height");
      $('#'+id).attr("src",loading_url);
       
	$.ajax({
			type: 'post',
			url: '/aviary_image_editor/process',
			data: "new="+new_url+"&old="+old_url+"&nid="+Drupal.settings.aviary_image_editor_return+"&external="+external+"&image="+image_stat+"&imagecache="+imagecache,
			timeout:10000,
			error: function(){
					 alert('Error Saving Image');
					$('#'+id).attr("src",old_url);
			},
			success: function(data) {
						var _ok=data.indexOf('/');
						if (_ok==-1)
						{ 
						   alert('Error Saving Image');
						   $('#'+id).attr("src",old_url);
						}else
						{
							$('#'+id).attr('src',data);
							$('#'+id).load(function(){
							
	
								var height_n= $('#'+id).height();
								var width_n= $('#'+id).width();
								//alert("new "+height_n+" by "+width_n);
								if((height_n>height)||(width_n>width))
								{
									$('#'+id).attr("width",width); 
									$('#'+id).attr("height",height);
									//alert("set "+height+" by "+width);
								}
							});
						}
						aviaryeditor_close(); 
					}

			});
      }
	  Feather_OnLoad = function() { 
        _featherLoaded = true; 
      } 
	  Feather_OnClose = function() { 
        _featherEditorLoaded = false;
      } 
		function addJavascript(jsname,pos) {
			var th = document.getElementsByTagName(pos)[0];
			var s = document.createElement('script');
			s.setAttribute('type','text/javascript');
			s.setAttribute('src',jsname);
			th.appendChild(s);
		} 
      function launchEditor(imageid) { 
	  var hostname = window.location.hostname;
	  hostname = hostname.replace("www.","").toLowerCase();
      old_url=$('#'+imageid).attr("src");
	 // if(old_url.indexOf("http://")!=-1 && old_url.indexOf(hostname)==-1)alert("External Image cannot be edited");
	 // else{
			if (_featherLoaded) { 
				_featherEditorLoaded=true;
				aviaryeditor(imageid, old_url); 
			}
	//	}
		return false;		
      } 
	$(document).ready(function () {
	   Feather_APIKey =  Drupal.settings.aviary_image_editor_api_key;//'62a1cc7e2d43094b8caf60127cc3374e';
	   Feather_OpenType = Drupal.settings.aviary_image_editor_open_type;//'float'; 

	   var prelaod = $('<img />').attr('src', Drupal.settings.aviary_image_editor_loading_url);
		//Check for access level
		if(Drupal.settings.aviary_image_editor_access == 'full'){	
			$('body').find('img:not('+Drupal.settings.aviary_image_editor_css_skip+')').each(function(){
				// Don't mess with admin-menu icons
				if(!$(this).parents('li').hasClass('admin-menu-icon')){
					add_link($(this));
				}
			});
		}else if(Drupal.settings.aviary_image_editor_access == 'restricted'){
			if(!Drupal.settings.aviary_image_editor_css_selector == ""){
				$(Drupal.settings.aviary_image_editor_css_selector).find('img:not('+Drupal.settings.aviary_css_skip+')').each(function(){
					// Don't mess with admin-menu icons
          if(!$(this).parents('li').hasClass('admin-menu-icon')){				
						add_link($(this));
					}
				});
			}
		}
		
		addJavascript('http://feather.aviary.com/js/feather.js','head'); 
		$('.aviary-img-wrap').hover(
			function(){
				if(_featherEditorLoaded==false)
				{
					var pic_link =	$(this).find('a');
					pic_link.fadeIn('fast');
					pic_link.click(function(){
						//location.assign(pic_link.attr('href'));
						return false;
					});
				}
			},
			function(){$(this).find('a').hide();}
		);
		
		if($('#picnick-preview-original').length && $('#picnick-preview-new').length){
			if($('#picnick-preview-original img').width() > $('#picnick-preview-original').width()){
				$('#picnick-preview-original img').width("100%");
			}
			if($('#picnick-preview-new img').width() > $('#picnick-preview-new').width()){
				$('#picnick-preview-new img').width("100%");
			}
		}
	});

	function add_link(img){
		var host = 'http://' + location.host;
		var link = host + '/aviary_image_editor/process/' + Drupal.settings.aviary_return;
		var path = "";
		var imageid;
				//launchEditor('editable')	
		if(img.hasClass('imagecache')){
			imagecache=true;
			path = img.attr('src');
			path.slice(0,6);
			var parts = path.split('/');
			for (x in parts) {
				if(parts[x] == "imagecache"){
					var imagecache = parts.splice(x,2);
					link = link.concat("/true");
					break;
				}
		  }
			path = parts.join('/');
		}else{
			path = img.attr('src');
			if(path && path.search(host) == -1){
				path = host.concat(path);
			}
		}
		// Only proceed if path is defined
		if(path){		
			imageid=img.attr("id");
	
			if(imageid.length==0)
			{
				imageid="aviary_"+Math.floor(Math.random()*10000);
				img.attr("id",imageid);
			}
			var request='launchEditor(\''+imageid+'\');';
			img.wrap("<span class='aviary-img-wrap'></span>");
			var overlay_w = $(window).width() - 20;
			var overlay_h = $(window).height() -20;
			var edit_icon_url=  Drupal.settings.aviary_image_editor_edit_icon;
			var icon ="<input type='image' value='Edit photo' src='"+edit_icon_url+"'  >"
			img.after("<a href='javascript:void(0);' onclick=\""+request+" return false;\"> "+icon+" </a>");		//Edit in aviary 	
		}
	}

}
