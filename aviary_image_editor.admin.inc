<?php
// $Id$
/**
 * @file
 * Configuration settings for aviary module
 */

/**
 * Settings form for the aviary module
 */
function aviary_image_editor_settings() {
  $form = array();

  $form['aviary_image_editor_api_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Aviary API Key'),
    '#default_value' => variable_get('aviary_image_editor_api_key', '62a1cc7e2d43094b8caf60127cc3374e'),
    '#description' => t('In order to use this module you need an API key from www.aviary.com. You can request one for free here: ' . l('Aviary API Key', 'http://www.aviary.com/html5builder')),
    '#required' => TRUE
  );

  $form['aviary_image_editor_tmp_path'] = array(
    '#type' => 'textfield',
    '#title' => t('Path for temporary files'),
    '#default_value' => variable_get('aviary_image_editor_tmp_path', file_directory_path()),
    '#maxlength' => 255,
    '#description' => t('A file system path where temporary aviary files will be stored.'),
    '#after_build' => array('system_check_directory'),
  );
  $form['aviary_image_editor_keep_backup'] = array(
    '#type' => 'checkbox',
    '#title' => t('Keep backup'),
    '#default_value' => variable_get('aviary_image_editor_keep_backup', true),
    '#description' => t('Keep a backup of the original image everytime it is edited.(would be saved as name_aviary_timestamp.ext)'),
  );
 $form['aviary_image_editor_open_type'] = array(
    '#type' => 'select',
    '#title' => t('Launch type'),
    '#default_value' => variable_get('aviary_image_editor_open_type','lightbox'),
    '#options' => array('lightbox' => t('Lightbox'),
						'float' => t('Float/Popup'),),
	'#description' => t('Select what type of widget editor to launch'),
  );
  $form['node_types'] = array(
    '#type' => 'fieldset',
    '#title' => t('aviary enabled node types'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#description' => t('Select the node types where the aviary option should be available.'),
  );

  $names = node_get_types('names');
  $form['node_types']['aviary_image_editor_node_types'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Node Types'),
    '#default_value' => variable_get('aviary_image_editor_node_types', array()),
    '#options' => $names,
  );

 
  return system_settings_form($form);
}